public class Pet {

    public int getNumLegs() {
        return numLegs;
    }

    public void setNumLegs(int numLegs) {
        this.numLegs = numLegs;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private int numLegs;
    private String name;

    public void feed () {
        System.out.println("Feed generic pet some generic pet food");
    }
}
