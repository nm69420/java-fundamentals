import com.sun.nio.sctp.SctpSocketOption;

public class Main {

    public static void main(String[] args) {

        Pet myPet = new Pet(); //comment
        myPet.setNumLegs(4);
        myPet.setName("Mycroft");

        System.out.println(myPet.getName());
        System.out.println(myPet.getNumLegs());
        myPet.feed();

        Dragon myDragon = new Dragon();
        myDragon.setName("Mycroft");

        Dragon usingNameConstructor = new Dragon("Derek");

        System.out.println(myDragon.getNumLegs());
        System.out.println(myDragon.getName());
        myDragon.feed();
        myDragon.breatheFire();

        Pet anotherReferenceToDragon;
        anotherReferenceToDragon = myDragon;

        myDragon.breatheFire();
    }
}
